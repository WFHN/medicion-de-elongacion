#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
#import numpy as np
from scipy.spatial import distance as dist
import selectfile
import cvui
import os
#from pathlib import Path  
#import bridge  

#import multitrack

#abrir()
selectfile
filename = selectfile.filename
#print(selectfile.filename)
#nombre_archivo = 


    
if filename:  
   # WINDOW_NAME	= 'Medicion de eje X'

    f = open("parametros.txt", "r")
    f_x = int(f.readline())
    f_y = int(f.readline())
    
    #f_x = 1424#996030 #px
    #f_y = 1424#25304 #px
    #c_x = 6271#30706 #px
    #c_y = 3820#68560 #px
    
    
    distancia_focal = float(f.readline()) #mm
    width_max = int(f.readline())
    sum_resoluciones = (240+144)
    
    increase_res_low = int(sum_resoluciones+int(f.readline()))
    decrease_res_low = int(sum_resoluciones-int(f.readline()))

    
    suma = (f_x+f_y)
    promedio = suma/2
    pix_mm  = promedio / distancia_focal #pixeles por milimetro
    
    
    if increase_res_low or sum_resoluciones == sum_resoluciones:
        resolution_prom= sum_resoluciones/2 #250 mejor resultado

    
    if increase_res_low > sum_resoluciones:
        resolution_prom= increase_res_low/2 #250 mejor res ultado
    else:# decrease_res_low <= sum_resoluciones:
        resolution_prom= decrease_res_low/2 #250 mejor resultado
    #if increase_res_low or decrease_res_low == 0:
       # resolution_prom= sum_resoluciones/2 #250 mejor resultado
  #  if decrease_res_low or decrease_res_low == sum_resoluciones:
        #resolution_prom= sum_resoluciones/2 
    #else: 
     #   resolution_prom= sum_resoluciones/2 #250 mejor resultado

    
  #  print(distancia_focal)
    pix_mm_solve = (resolution_prom*pix_mm)/width_max
    
    res_low = pix_mm_solve
    
    #res_low = 6.1028565# 712312267.55 #640p
     # 144p  140 >>> 6.67499984375
    #240 > 11.44285# 720 > 34.32857 #30.514  320 >#15.2571425  640> 30.514285 # 1280/m = 640 /x
    #print (resolution_calib)
    #print(promedio)
   # print(pix_mm)
  # print(decrease)
    #print(res_low)
    print ("resolution prom :"+ str(resolution_prom))
  #  print ("decrease "+str(decrease_res_low))
   # print ("increase "+str(increase_res_low))

   # print("ecuacion resuelta "+str(pix_mm_solve))
    
    #calcular distancia entre 2 puntos
    
    drawing = False
    point1 = ()
    point2 = () #vacias porque deben ser asignadas
    d=[]#guardar distancias
    
    def midpoint(pointA, pointB): #determina punto medio
        return ((pointA[0] + pointB[0]) * 0.5, (pointA[1] + pointB[1]) * 0.5) 
    
    def mouse_drawing(event, x, y, flags, params): #dibuja linea en imagen
        global point1, point2, drawing
        if event == cv2.EVENT_LBUTTONDOWN:
            if drawing is False:
                drawing = True
                point1 = (x, y)
            else:
                drawing = False
                dA = dist.euclidean((int(point1[0]), int(point1[1])), (int(point2[0]), int(point2[1])))
                d.append(dA)
               # print(d)
     
        elif event == cv2.EVENT_MOUSEMOVE:
            if drawing is True:
                point2 = (x, y)
                
    #calibrationdata = np.load('result.npz')
    #mtx = calibrationdata['mtx']
    #dist = calibrationdata['dist']
               
    cap = cv2.VideoCapture(filename)
    cv2.namedWindow("Frame")
    cv2.setMouseCallback("Frame", mouse_drawing)
    # while cap.isOpened():
    success, frame = cap.read()
   # frame = cv2.resize(frame, (1280, 720))                    # Resize image

    bgr  = cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
    bgr  = cv2.resize(bgr, (1280, 720))                    # Resize image

    cvui.text(bgr, 50, 30, "Presione SPACE para proseguir",0.5,2552360)
    cvui.text(bgr, 50, 50, "Presione ESC para salir ", 0.5,2552360)
    while True:
       # if cvui.button(gray, 50, 200, "SALIR"):
        #    print ("Hola mundo")
        
       # success, frame = cap.read()
        #dst = cv2.undistort(frame, mtx, dist, None, newcameramtx)
    
        if point1 and point2:
            bgr  =  cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
            bgr  = cv2.resize(bgr, (1280, 720))                    # Resize image

            cv2.line(bgr, point1, point2, (0, 255, 0))
            dA = dist.euclidean((int(point1[0]), int(point1[1])), (int(point2[0]), int(point2[1])))
            (tX, tY) = midpoint(point1, point2)
            medicion = float(dA/res_low)#mm
          #  cv2.putText(frame, "{:.1f}px".format(dA),(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (255, 255, 255), 1)
            cv2.putText(bgr, str(medicion)+" mm",(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (0, 0, 0), 1)
           # cv2.putText(gray, str(medicion)+" mm",(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (255, 255, 255), 1)

            #print ("milimetros "+str(medicion))

            cvui.text(bgr, 50, 10, "Eje X: "+str(medicion)+ " mm.",0.5, 2552360)
            cvui.text(bgr, 50, 30, "Presione SPACE para proseguir",0.5,2552360)
            cvui.text(bgr, 50, 50, "Presione ESC para salir ", 0.5,2552360)


           # WINDOW_NAME = 'Medicion de elongacion'
            
     
            
    
     #   print (point1)
      #  print (point2)
        #cvui.init(WINDOW_NAME, 20)
        #cvui.update()

       # if cvui.button(gray, 50, 200, "SALIR"):
         #   print ("Hola mundo")
        cv2.imshow("Frame", bgr)
     
        key = cv2.waitKey(1)
        if key == 27: #esc
            os._exit(0)

        if key == 32: #ESPAACE
            #temp_video = open("video_temp.txt", "w+")
            text_file = open("medicion_eje_x.txt", "w+")
            text_file_2 = open("path_temp.txt", "w+")

            text_file.write(str(medicion))
            text_file_2.write(str(filename))


            text_file.close()
            text_file_2.close()

            #multitrack.run_tracking()
            #print ("SPACE")
            
            #cap.release()
            #cv2.destroyAllWindows()
            #quit()

            break

   
    cv2.imshow("Frame", frame)
    
    #print ("param1 = "+param1)
    #print ("param2 = "+param2)

    cap.release()

    cv2.destroyAllWindows()
    f.close()
    exec(open("multitrack.py").read())
 
    #calibrationdata.close()

else:
    print ("Cancelado por el usuario")









