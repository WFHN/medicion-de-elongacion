


import heapq
import sys
import cv2
#from random import randint
from scipy.spatial import distance as dist
import cvui
import os
import matplotlib.pyplot as plt 
from openpyxl import Workbook
from openpyxl import load_workbook
#from scipy.signal import find_peaks
#import scipy.signal
#import numpy as np


#import numpy as np
import selectfile
#from pathlib import Path  
#import bridge  

#import multitrack


    
def multitracker():    
    #trigger = False
    mediciones = [] 
    
    #import selectfile
           
    def destruir():
       os._exit(0)
       #sys.exit("Terminado por usuario")
       # reload()
    
    nombre_archivo = open("path_temp.txt", "r")
    parametros = open("parametros.txt", "r")
    f_x = int(parametros.readline())
    f_y = int(parametros.readline())
    name_file_pre = str(nombre_archivo.readline())
    name_file = str(os.path.basename(name_file_pre))
    
    
    
    
    distancia_focal = float(parametros.readline()) #mm
    width_max = int(parametros.readline())
    sum_resoluciones = (240+144)
    
    increase_res_low = int(sum_resoluciones+int(parametros.readline()))
    decrease_res_low = int(sum_resoluciones-int(parametros.readline()))
    
    suma = (f_x+f_y)
    promedio = suma/2
    pix_mm  = promedio / distancia_focal #pixeles por milimetro
    
    if increase_res_low or sum_resoluciones == sum_resoluciones:
            resolution_prom= sum_resoluciones/2
            
    if increase_res_low > sum_resoluciones:
        resolution_prom = increase_res_low/2 #250 mejor resultado ignorar comentario
    else:
        resolution_prom = decrease_res_low/2 #250 mejor resultado ignorar comentario
    #if increase_res_low or decrease_res_low == 0:
       # resolution_prom= sum_resoluciones/2 #250 mejor resultado ignorar comentario
        
    #resolution_prom= (210+144)/2   #20 unidades 720 260 210 para 1080 ignorar comentario
    
    
    pix_mm_solve = (resolution_prom*pix_mm)/1920
    res_low = pix_mm_solve
    
    f = open("path_temp.txt", "r")
    filename = f.readline()
    #print (filename)
    WINDOW_NAME	= 'Medicion de elongacion'
    
    
    trackerTypes = ['BOOSTING', 'MIL', 'KCF','TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']
    
    def createTrackerByName(trackerType):
      # crear tracksegun tipo de tracker
      if trackerType == trackerTypes[0]:
        tracker = cv2.TrackerBoosting_create()#inexacto
      elif trackerType == trackerTypes[1]: 
        tracker = cv2.TrackerMIL_create()#inestable
      elif trackerType == trackerTypes[2]:
        tracker = cv2.TrackerKCF_create()#inexacto
      elif trackerType == trackerTypes[3]:
        tracker = cv2.TrackerTLD_create()#inestable
      elif trackerType == trackerTypes[4]:
        tracker = cv2.TrackerMedianFlow_create() #rapido no preciso, performance ok
      elif trackerType == trackerTypes[5]:
        tracker = cv2.TrackerGOTURN_create()#inestable
      elif trackerType == trackerTypes[6]:
        tracker = cv2.TrackerMOSSE_create() #rapidez poca precision
      elif trackerType == trackerTypes[7]:
        tracker = cv2.TrackerCSRT_create() #precision poca rapidez
      else:
        tracker = None
     #   print('Tracker no existe')
      #  print('Trackers disponibles:')
        #for t in trackerTypes:
         # print(t)
        
      return tracker
    
    
    if __name__ == '__main__':
    
    
      
     # print("Default tracking algoritm is CSRT \n"
        #    "Available tracking algorithms are:\n")
      for t in trackerTypes:
         # print(t)      
    
        trackerType = "CSRT" #Tracker seleccionado
           
      
    
      # capturar imagenes
      #cap = cv2.VideoCapture(2)
      cap = cv2.VideoCapture(filename)
     
      #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 320);
      #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 240);
      #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
      #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
      
     
      # leer primer frame
      exito, frame = cap.read()
      #frame  =  cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
    
      frame = cv2.resize(frame, (1280, 720))                    # Resize image
    
      #capturar framerate
      framerate = cap.get(cv2.CAP_PROP_FPS)
      framecount = 0
      
      #fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);
    #  ret, orig_frame = video.read()
      #cv2.imshow('Vista Previa', cap)
    
      # salir 
      
      
      if not exito:
        print('Falla al capturar video')
        sys.exit(1)
        
    
      ## Seleccionar boxes
      bboxes = []
      colors = [] 
      #cvui.text(frame, 50, 80, "HOLAAA: ")
      #k = cv2.waitKey(1) & 0xff
      #if k == 27 : 
         # quit()
               # break
               # cap.release()
               # cv2.destroyAllWindows()
    
      # OpenCV no permite seleccionar multiples ROI
      # Se debe iterar para colecionar los ROI
      
      while True:
         
       # WINDOW_NAME = 'Medicion de elongacion'
       # cvui.init(WINDOW_NAME, 20)
       # cvui.update()
    
       # if cvui.button(frame, 50, 200, "SALIR"):
          #  break
       # cv2.imshow(WINDOW_NAME, frame)
        contador = 0
        fromCenter = True
        
        bbox = cv2.selectROI('Medicion de elongacion', frame, True , fromCenter)
        bboxes.append(bbox)
      
        
        print("Press q to quit selecting boxes and start tracking")
        print("Press any other key to select next object")
        k = cv2.waitKey(0) & 0xFF
        #kill = cv2.waitKey(1) & 0xff
        #if (kill == 107) : 
           # os._exit(0)
           # cap.release()
           # cv2.destroyAllWindows()
               
       
            
        if (k==27):
            destruir()
            
      
        if (len(bboxes) == 2):  # q is pressed
            print("Inicio de tracking")
            break
       # colors.append((randint(64, 255), randint(64, 255), randint(64, 255)))
        #cv2.rectangle(frame, p1, p2, colors[i], 2, 1)
       # contador = int(len(bboxes))
       # print(contador)
        
            # mostrar frames
        
        #k = cv2.waitKey(0) & 0xFF
       # if (k == 113):
        #   break
       
     
      
      # crear objeto multitracker
      multiTracker = cv2.MultiTracker_create()
    
      # inicializar multitracker 
      for bbox in bboxes:
        multiTracker.add(createTrackerByName(trackerType), frame, bbox)
    
    
      # procesar video y trackear objetos
      while cap.isOpened():
        exito, frame = cap.read()
        #frame  =  cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
    
        if exito:
            frame = cv2.resize(frame, (1280, 720))                    # Resize image
    
    
        listaCentroides = []
        framecount += 1
        #timer = cv2.getTickCount()
    
    
        if not exito:
          break
        
        # obtener ubicacion actulazida de los frames subsecuentes
        exito, boxes = multiTracker.update(frame)
    
        # dibujar centroide
        for i, newbox in enumerate(boxes):
          p1 = (int(newbox[0]), int(newbox[1]))
          p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
        #  p3 = (int(newbox[0]), int(newbox[1]))
       #   p4 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
          #cv2.rectangle(frame, p1, p2, colors[i], 2, 1)
          #cv2.rectangle(frame, p3, p4, colors[i], 2, 1)
    
          
          cX = int((int(p1[0])+ int(p2[0])) /2.0)
          cY = int((int(p2[1])+ int(p1[1]))/2.0 )
         # cX_2 = int((int(p3[0])+ int(p4[0])) /2.0)
         # cY_2 = int((int(p3[1])+ int(p4[1]))/2.0 )
        
          centroide = ((cX, cY))
          cv2.circle(frame, centroide , 5,  (0,255,0), 2, -1)
          
          #calcular distancia entre 2 puntos
    
          drawing = False
           #vacias porque deben ser asignadas
         #print("punto 1 es : "+ str(point1))
          #print('punto 2 es  '+ str(point2))
          
          #guardar centroides en lista de tuplas
         # d=[]#guardar distancias
          listaCentroides.append(centroide)
          
        
    
        
        
        def midpoint(pointA, pointB): #determina punto medio
              return ((pointA[0] + pointB[0]) * 0.5, (pointA[1] + pointB[1]) * 0.5) 
          
        point1 = (listaCentroides[0]) #eje y
        point2 = (listaCentroides[1]) #eje y
       # point3 = (listaCentroides[2]) #eje x
       # point4 = (listaCentroides[3]) #eje x
          #if point1 and point2:
        cv2.line(frame, point1, point2, (0, 255, 0))
       # cv2.line(frame, point3, point4, (0, 255, 255))
    
        dA = dist.euclidean((int(point1[0]), int(point1[1])), (int(point2[0]), int(point2[1])))
        (tX, tY) = midpoint(point1, point2)
        medicion = float(dA/res_low)#mm
    
        cv2.putText(frame, str(medicion)+" mm",(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (0, 0, 0), 1)
       
       # dA_2 = dist.euclidean((int(point3[0]), int(point3[1])), (int(point4[0]), int(point4[1])))
      #  (tX_2, tY_2) = midpoint(point3, point4)
      #  cv2.putText(frame, "{:.1f}px".format(dA_2),(int(tX_2 - 25), int(tY_2 - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (255, 255, 255), 1)
       # cvui.text(frame, 50, 80, "Eje X: {:.1f}px".format(dA))
        #cvui.text(frame, 50, 100, "Eje Y: {:.1f}px".format(dA_2))
       # cvui.text(frame, 50, 80, "Eje Y: "+str(dA), 0.7, 2552360)
      #  cvui.text(frame, 50, 100, "Eje X: "+str(dA_2))
        cvui.text(frame, 50, 80, "Frame: "+str(framecount),0.5, 2552360)
    
       # print("Eje Y: "+str(medicion))
        mediciones.append(medicion)
     #   print("Eje X: "+str(dA_2))
       # cvui.button(frame, 50, 120, 'salir')
        
        #print (dA)
    
        #print('sacado 1  de la lista de centroides '+str(listaCentroides[0]))
       # print('lista centroides '+str(listaCentroides))
       # print('sacado 2  de la lista de centroides '+str(listaCentroides[1]))
        # quit on ESC button
        # mostrar frames
        WINDOW_NAME = 'Medicion de elongacion'
        cvui.init(WINDOW_NAME, 20)
        cvui.update()
        if cvui.button(frame, 50, 100, "ABORTAR"):
          #  trigger = True
            break
       # cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
     #   cv2.setWindowProperty(WINDOW_NAME,cv2.WND_PROP_FULLSCREEN,cv2.WND_PROP_FULLSCREEN)
        #cv2.resizeWindow(WINDOW_NAME, 600,600)
     #   cv2.namedWindow(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN)
        #cv2.setWindowProperty(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        cv2.imshow(WINDOW_NAME, frame)
        #cv2.setWindowProperty(WINDOW_NAME, cv2.CV.WND_PROP_FULLSCREEN, cv2.CV.WINDOW_FULLSCREEN)
    
    
    
      #  if cvui.button(frame, 50, 200, 'salir'):
          #  break
        
        
        #if framecount == (framerate * 2):
         # framecount = 0
          #cv2.imshow(WINDOW_NAME, frame)
       
     #   key = cv2.waitKey(1)
       # if key == 27:
        #    break
    
    cap.release()
    cv2.destroyAllWindows()
    f.close()
    parametros.close()
        
    plt.plot(mediciones)
    plt.savefig('Graficas/'+name_file+'_snapshot.png', dpi=100)
    plt.show()
    plt.close()
     
   
    
    
    if len(mediciones) > 100:
    
        maximos = heapq.nlargest(100, mediciones)
        suma_promedio_maximos = (float(maximos[0])+float(maximos[99]))
        promedio_maximos = float(suma_promedio_maximos/2)
        
        percent_elong_pre = float((promedio_maximos-mediciones[0])/mediciones[0])
        percent_elong = float(percent_elong_pre * 100)
        
       # porcentual_cent = float(mediciones[0]/promedio_maximos) * 100)
        #diferencia_porcentual = 100 - porcentual_cent
        tercio = percent_elong / 3
        
        conversion_pre = tercio / 100
        
        eje_x = open("medicion_eje_x.txt", "r")
        eje_x_data = float(eje_x.readline())
    
        contraccion = conversion_pre * eje_x_data
        
        contraccion_horizontal = float(eje_x_data - contraccion)
      #  print (maximos)
      #  print("vertical original "+str(mediciones[0]))
      #  print ("elongacion vertical "+str(promedio_maximos))
       # print (porcentual_cent)
      #  print("porcentaje elongacion : "+str(percent_elong))
        
      #  print("horizontal original "+str(eje_x_data))
      #  print("contraccion seccion horizontal : "+str(contraccion_horizontal))
        
        lateral_strain = float((eje_x_data-contraccion_horizontal)/eje_x_data)
        linear_strain = float((promedio_maximos-mediciones[0])/mediciones[0])
        poisson = float(lateral_strain/linear_strain)
        
        lateral_strain_solid = float((eje_x_data-contraccion_horizontal)/eje_x_data)
        linear_strain_solid =  float((maximos[0]-mediciones[0])/mediciones[0])
      #  poisson_solid = float(lateral_strain_solid/linear_strain_solid)
    
       # print("maximo solid "+str(maximos[0]))
       # print ("coeficiente de poisson : "+str(poisson))
       # print("coeficiente poisson solido: "+str(poisson_solid))
        eje_x.close()
        #guardar graficas
        
        #guardar datos en excel
        
        filename = "datos.xlsx"
        new_row = [name_file, str(eje_x_data), str(contraccion_horizontal), str(mediciones[0]), str(promedio_maximos),str(percent_elong), str(poisson)]
        
        try:
            wb = load_workbook(filename)
            ws = wb.worksheets[0]  # select first worksheet
        except FileNotFoundError:
            headers_row = ['Archivo', 'Eje x', 'Contraccion x', 'Eje y', 'Elongacion y' ,'Porcentaje elongacion','Poisson' ]
            wb = Workbook()
            ws = wb.active
            ws.append(headers_row)
    
        ws.append(new_row)
        wb.save(filename)
        
        
        
        
        nombre_archivo.close()
    
    #dOrder=sorted(maximos)
    '''
    n=50
    middle=n/2
    
    # codigo para calcular la mediana
    if n%2==0:
    	mediana=(maximos[middle+1] + maximos[middle+2]) / 2
    else:
    	mediana=maximos[middle+1]*1
    
    print ('')
    print ('Total datos', n)
    print ('Mediana: ', mediana)
    '''

#abrir()
selectfile
filename = selectfile.filename
#print(selectfile.filename)
#nombre_archivo = 


    
if filename:  
   # WINDOW_NAME	= 'Medicion de eje X'

    f = open("parametros.txt", "r")
    f_x = int(f.readline())
    f_y = int(f.readline())
    
    #f_x = 1424#996030 #px
    #f_y = 1424#25304 #px
    #c_x = 6271#30706 #px
    #c_y = 3820#68560 #px
    
    
    distancia_focal = float(f.readline()) #mm
    width_max = int(f.readline())
    sum_resoluciones = (240+144)
    
    increase_res_low = int(sum_resoluciones+int(f.readline()))
    decrease_res_low = int(sum_resoluciones-int(f.readline()))

    
    suma = (f_x+f_y)
    promedio = suma/2
    pix_mm  = promedio / distancia_focal #pixeles por milimetro
    
    
    if increase_res_low or sum_resoluciones == sum_resoluciones:
        resolution_prom= sum_resoluciones/2 #250 mejor resultado

    
    if increase_res_low > sum_resoluciones:
        resolution_prom= increase_res_low/2 #250 mejor res ultado
    else:# decrease_res_low <= sum_resoluciones:
        resolution_prom= decrease_res_low/2 #250 mejor resultado
    #if increase_res_low or decrease_res_low == 0:
       # resolution_prom= sum_resoluciones/2 #250 mejor resultado
  #  if decrease_res_low or decrease_res_low == sum_resoluciones:
        #resolution_prom= sum_resoluciones/2 
    #else: 
     #   resolution_prom= sum_resoluciones/2 #250 mejor resultado

    
  #  print(distancia_focal)
    pix_mm_solve = (resolution_prom*pix_mm)/width_max
    
    res_low = pix_mm_solve
    
    #res_low = 6.1028565# 712312267.55 #640p
     # 144p  140 >>> 6.67499984375
    #240 > 11.44285# 720 > 34.32857 #30.514  320 >#15.2571425  640> 30.514285 # 1280/m = 640 /x
    #print (resolution_calib)
    #print(promedio)
   # print(pix_mm)
  # print(decrease)
    #print(res_low)
 #   print ("resolution prom :"+ str(resolution_prom))
  #  print ("decrease "+str(decrease_res_low))
   # print ("increase "+str(increase_res_low))

   # print("ecuacion resuelta "+str(pix_mm_solve))
    
    #calcular distancia entre 2 puntos
    
    drawing = False
    point1 = ()
    point2 = () #vacias porque deben ser asignadas
    d=[]#guardar distancias
    
    def midpoint(pointA, pointB): #determina punto medio
        return ((pointA[0] + pointB[0]) * 0.5, (pointA[1] + pointB[1]) * 0.5) 
    
    def mouse_drawing(event, x, y, flags, params): #dibuja linea en imagen
        global point1, point2, drawing
        if event == cv2.EVENT_LBUTTONDOWN:
            if drawing is False:
                drawing = True
                point1 = (x, y)
            else:
                drawing = False
                dA = dist.euclidean((int(point1[0]), int(point1[1])), (int(point2[0]), int(point2[1])))
                d.append(dA)
               # print(d)
     
        elif event == cv2.EVENT_MOUSEMOVE:
            if drawing is True:
                point2 = (x, y)
                
    #calibrationdata = np.load('result.npz')
    #mtx = calibrationdata['mtx']
    #dist = calibrationdata['dist']
               
    cap = cv2.VideoCapture(filename)
    cv2.namedWindow("Frame")
    cv2.setMouseCallback("Frame", mouse_drawing)
    # while cap.isOpened():
    success, frame = cap.read()
   # frame = cv2.resize(frame, (1280, 720))                    # Resize image

    bgr  = cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
    bgr  = cv2.resize(bgr, (1280, 720))                    # Resize image

    cvui.text(bgr, 50, 30, "Presione SPACE para proseguir",0.5,2552360)
    cvui.text(bgr, 50, 50, "Presione ESC para salir ", 0.5,2552360)
    while True:
       # if cvui.button(gray, 50, 200, "SALIR"):
        #    print ("Hola mundo")
        
       # success, frame = cap.read()
        #dst = cv2.undistort(frame, mtx, dist, None, newcameramtx)
    
        if point1 and point2:
            bgr  =  cv2.cvtColor(frame, cv2.COLOR_XYZ2RGB)
            bgr  = cv2.resize(bgr, (1280, 720))                    # Resize image

            cv2.line(bgr, point1, point2, (0, 255, 0))
            dA = dist.euclidean((int(point1[0]), int(point1[1])), (int(point2[0]), int(point2[1])))
            (tX, tY) = midpoint(point1, point2)
            medicion = float(dA/res_low)#mm
          #  cv2.putText(frame, "{:.1f}px".format(dA),(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (255, 255, 255), 1)
            cv2.putText(bgr, str(medicion)+" mm",(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (0, 0, 0), 1)
           # cv2.putText(gray, str(medicion)+" mm",(int(tX - 25), int(tY - 20)), cv2.FONT_HERSHEY_SIMPLEX,0.65, (255, 255, 255), 1)

            #print ("milimetros "+str(medicion))

            cvui.text(bgr, 50, 10, "Eje X: "+str(medicion)+ " mm.",0.5, 2552360)
            cvui.text(bgr, 50, 30, "Presione SPACE para proseguir",0.5,2552360)
            cvui.text(bgr, 50, 50, "Presione ESC para salir ", 0.5,2552360)


           # WINDOW_NAME = 'Medicion de elongacion'
            
     
            
    
     #   print (point1)
      #  print (point2)
        #cvui.init(WINDOW_NAME, 20)
        #cvui.update()

       # if cvui.button(gray, 50, 200, "SALIR"):
         #   print ("Hola mundo")
        cv2.imshow("Frame", bgr)
     
        key = cv2.waitKey(1)
        if key == 27: #esc
            os._exit(0)

        if key == 32: #ESPAACE
            #temp_video = open("video_temp.txt", "w+")
            text_file = open("medicion_eje_x.txt", "w+")
            text_file_2 = open("path_temp.txt", "w+")

            text_file.write(str(medicion))
            text_file_2.write(str(filename))


            text_file.close()
            text_file_2.close()

            #multitrack.run_tracking()
            #print ("SPACE")
            
            #cap.release()
            #cv2.destroyAllWindows()
            #quit()

            break

   
    cv2.imshow("Frame", frame)
    
    #print ("param1 = "+param1)
    #print ("param2 = "+param2)

    cap.release()

    cv2.destroyAllWindows()
    f.close()
    multitracker()
 
    #calibrationdata.close()

else:
    print ("Cancelado por el usuario")