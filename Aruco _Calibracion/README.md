# Calibrate camera with arUco markers using opencv and python. 
(Code developed and tested on opencv 3.3.1, 3.7.x)


# camera_calibration
Code and resources for camera calibration using arUco markers and opencv 

1. Imprimir el tablero adjunto
2. Tome como minimo 50 fotografias del tablero en disstintos angulos.  Use data_generation. py si necesita capturar fotos o bien use la herramienta nativa de su SO.
3. Guarde las fotos en la carpeta aruco_data.
(Ejemplos contenidos en la carpeta)
4. Los datos se guardan en calibration.yaml
5. Use openyaml.py para visualizar los datos de calibracion.

